import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import select from '@material-ui/core/Select';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class AffichageConsole extends Component {
    constructor() {

        super();

        this.state = {
          userInput:'',
          userInputpren:'',
          userInputMail:'',
          userInputPwd:'',
          userInputDate:'',
          userInputAd:'',
          userInputVille:'',
          userInputSexe:'',
          items:[],
          open:false,

        };

}
onChange(e){

  this.setState({
    userInput: e.target.value,
    userInputpren:e.target.value,
    userInputMail:e.target.value,
    userInputPwd:e.target.value,
    userInputDate:e.target.value,
    userInputAd:e.target.value,
    userInputVille:e.target.value,
    userInputSexe:e.target.value
  });

}
Ajouter(e){
e.preventDefault();
this.setState({
  userInput:'',
  userInputpren:'',
  userInputMail:'',
  userInputPwd:'',
  userInputDate:'',
  userInputAd:'',
  userInputVille:'',
  userInputSexe:'',
items : [...this.state.items, this.state.userInput,this.state.userInputpren,this.state.userInputMail,this.state.userInputPwd,this.state.userInputDate,this.state.userInputAd,this.state.userInputVille,this.state.userInputSexe]
});
}
renderTodos(){
  return this.state.items.map((item)=>{
    return(
      <div key={item} className="div-affichage">
        {item}|<Button variant="contained" color="primary" onClick={this.deleteTodo.bind(this)} >Supprimer</Button>
      </div>
    );
  });
}

deleteTodo(item) {
        // no event
        // pass the item to indexOf
        const array = this.state.items;
        const index = array.indexOf(item);
        array.splice(index, 1);
        this.setState({
            items: array
        });
    }



      handleClickOpen = () => {
        this.setState({ open: true });
      };

      handleClose = () => {
        this.setState({ open: false });
      };





render() {

  return(

<div>
<h1 align ="center">Formulaire d'ajout</h1>
<form className="form-group" onSubmit={this.handleSubmit}>


  <TextField

    label="Nom"
    id="name"
    placeholder="Entrer votre nom complet"
    className="form-control mb-2"
    value={this.state.userInput}
    onChange={(e) => this.setState({ userInput : e.target.value })}
    margin="normal"
    type="text"
  />



    <TextField

      label="prenom"
      id="prenom"
      placeholder="Entrer votre prenom complet"
      className="form-control mb-2"
      value={this.state.userInputpren}
      onChange={(e) => this.setState({ userInputpren : e.target.value })}
      margin="normal"
      type="text"
    />



      <Button onClick={this.handleClickOpen}>Email</Button>
      <Dialog
      open={this.state.open}
      onClose={this.handleClose}
      aria-labelledby="form-dialog-title"
      >
      <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
      <DialogContent>
            <DialogContentText>
             please enter your email address here
            </DialogContentText>
                  <TextField
                    placeholder="Mail"
                    value={this.state.userInputMail}
                    onChange={(e) => this.setState({ userInputMail : e.target.value })}
                    className="form-control mb-2"
                    autoFocus
                    margin="dense"
                    id="email"
                    label="Email Address"
                    type="email"
                    fullWidth
                    />
        </DialogContent>
        <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                Annuler
                </Button>
                <Button onClick={this.handleClose} color="primary">
                  OK
                </Button>
          </DialogActions>
          </Dialog>



        <TextField

               label="Password"
              id="Password"
              placeholder="Entrer votre mot de passe"
              className="form-control mb-2"
              value={this.state.userInputPwd}
              onChange={(e) => this.setState({ userInputPwd : e.target.value })}

              margin="normal"
              type="password"
            />

            <TextField
               label="DAte De Naissance"
              id="Date"
              className="form-control mb-2"

              value={this.state.userInputDate}
              onChange={(e) => this.setState({ userInputDate : e.target.value })}

              margin="normal"
              type="date"
              InputLabelProps={{
             shrink: true,
        }}
            />


            <TextField

               label="Adresse"
              id="Adresse"
              placeholder="1234 Main St"
              className="form-control mb-2"
             value={this.state.userInputAd}
             onChange={(e) => this.setState({ userInputAd : e.target.value })}

              margin="normal"
              type="text"/><br></br>



              <TextField label="Ville"/>
                  <select className="form-control mb-2" value={this.state.userInputVille} onChange={(e) => this.setState({ userInputVille : e.target.value })}>
                      <option >Choisis...</option>
                      <option>Tunis</option>
                        <option>Nabeul</option>
                          <option>Beja</option>
                            <option>Sousse</option>
                              <option>Sfax</option>
                                <option>sidi Bouzid</option>
                                  <option>Médenine</option>
                    </select><br></br>




  <TextField label="Sexe" className="Sx"/ >

                             <RadioGroup
                               aria-label="gender"
                               name="gender2"
                               className="form-control mb-2"
                                value={this.state.userInputSexe} onChange={(e) => this.setState({ userInputSexe : e.target.value })}
                             >
                               <FormControlLabel
                                 value="female"
                                 control={<Radio color="primary" />}
                                 label="Female"
                                 labelPlacement="start"
                               />
                               <FormControlLabel
                                 value="male"
                                 control={<Radio color="primary" />}
                                 label="Male"
                                 labelPlacement="start"
                               />

</RadioGroup>


    <Button  onSubmit={this.handleSubmit} variant="contained" color="primary" className="btn btn-primary" onClick={ () => console.log(this.state)}>
          Ajouter

        </Button>
        <Button  onSubmit={this.handleSubmit} variant="contained" color="primary" onClick={this.Ajouter.bind(this)}>
              Afficher

            </Button>


</form>


<div>
  {this.renderTodos()}
</div>

</div>




);
}
}
export default AffichageConsole;
