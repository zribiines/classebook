import React, { Component } from 'react';
import { HashRouter as Router, Route, NavLink } from 'react-router-dom';
import SignUpForm from './pages/SignUpForm';
import SignInForm from './pages/SignInForm';
import AffichageConsole from './pages/AffichageConsole';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App" >

          <div className="App__Aside">

            <NavLink to="/AjouterPersonne" activeClassName="App__Aside--Active" className="App__Aside__Item">Ajouter Personne</NavLink>





          </div>

          <div className="App__Form">
            <div className="PageSwitcher">
                <NavLink to="/sign-in" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">Se Connecter</NavLink>
                <NavLink exact to="/" activeClassName="PageSwitcher__Item--Active" className="PageSwitcher__Item">S'inscrire</NavLink>

              </div>

              <div className="FormTitle">
                  <NavLink to="/sign-in" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Se connecter</NavLink> ou <NavLink exact to="/" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">S'inscrire</NavLink>
              </div>

              <Route exact path="/" component={SignUpForm}>
              </Route>
              <Route path="/sign-in" component={SignInForm}>
              </Route>
              <Route exact path="/AjouterPersonne" component={AffichageConsole}>
              </Route>





          </div>

        </div>

      </Router>
    );
  }
}

export default App;
